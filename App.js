import React, {Component} from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { WebView } from 'react-native';
// uri: 'https://www.youtube.com/embed/tgbNymZ7vqY?playsinline=1'
export default class App extends Component {
  render() {
    return (
      <View style={styles.view}>
          <WebView
              style={styles.videoWebView}
              javaScriptEnabled={true}
              source={{uri: 'https://www.youtube.com/embed/tgbNymZ7vqY?playsinline=1'}}
              allowsInlineMediaPlayback={true}
              originWhitelist={['*']}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create(
    {
        videoWebView: {
            backgroundColor: 'transparent',
            width: Dimensions.get('window').width,
            /* height: Dimensions.get('window').height /3, */
            marginLeft: 0,
            marginTop: 100,
            marginBottom: 16
        }, view: {
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
        }
    }
);
